﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Utilties
{
    public static class FileUtil<T>
    {
        /// <summary>
        /// Converts a UTF-8 encoded text file with a list of values seperated by a new line to a list of values.
        /// </summary>
        /// <param name="path">The path to the file to be converted.</param>
        /// <param name="conversionFunction">The function to convert the value string to the required type for the list.</param>
        public static List<T> ConvertInputFileToList(string path, Func<string, T> conversionFunction)
        {
            List<T> output = new List<T>();
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    output.Add(conversionFunction(s));
                }
            }
            return output;
        }
    }
}
