﻿using Utilties;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            Execute();
            stopWatch.Stop();
            Console.WriteLine($"Execution Time: {stopWatch.Elapsed}");
            Console.Read();
        }

        private static void Execute()
        {
            var inputFrequencies = FileUtil<int>.ConvertInputFileToList(@"../../../1.txt", (string value) => Convert.ToInt32(value));

            var repeatedFrequency = GetRepeatedFrequency(inputFrequencies);
            if (repeatedFrequency != null)
            {
                Console.WriteLine($"Found a repeat frequency: {repeatedFrequency}");
            }
            else
            {
                Console.WriteLine("Count not find a repeated frequency.");
            }
        }

        private static long? GetRepeatedFrequency(IEnumerable<int> frequencies, uint maxIterations = uint.MaxValue)
        {
            long currentFrequency = 0;
            var previousFrequencies = new List<long> { currentFrequency };

            for (uint i = 0; i < maxIterations; i++)
            {
                foreach (var frequency in frequencies)
                {
                    currentFrequency += frequency;
                    if (previousFrequencies.Contains(currentFrequency))
                    {
                        return currentFrequency;
                    }
                    else
                    {
                        previousFrequencies.Add(currentFrequency);
                    }
                }
            }

            return null;
        }
    }
}
