﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utilties;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputs = FileUtil<string>.ConvertInputFileToList(@"../../../2.txt", str => str);

            var list2 = new List<string>();
            var list3 = new List<string>();

            foreach (var input in inputs)
            {
                if(input.GroupBy(x => x).Any(g => g.Count() == 2))
                {
                    list2.Add(input);
                }
                if (input.GroupBy(x => x).Any(g => g.Count() == 3))
                {
                    list3.Add(input);
                }
            }

            Console.WriteLine(list2.Count * list3.Count);
            Console.Read();
        }
    }
}
